<?php
namespace XGWeb;
use ArrayObject;

/**
 * Date/Time Utilities
 *
 * @author Rob Steiner
 * @copyright  2016 Project Ana, Inc.
 *
 * Copyright (C) Project Ana, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Rob Steiner <rob@projectana.com>, February 2016
 */
 
class Collection extends ArrayObject {
    const DESC = 'DESC';
    const ASC = 'ASC';

    protected $_filtered_array = [];
    protected $_removed = [];

    public function get($key) {
        return $this->offsetExists($key) ? $this->offsetGet($key) : null;
    }
    
    function addKey($key, $value): self
    {
        $this[$key] = $value;
        
        return $this;
    }

    public function getObjectIds(string $key = 'object'): self
    {
        $return = [];
        foreach ($this as $object) {
            if (!is_object($object)) {
                break;
            }

            $return[] = $object->$key;
        }

        return new self($return);
    }

    public function getValuesByKey(string $key = 'object'): self
    {
        $return = [];
        foreach ($this as $item) {
            $return[] = $item[$key];
        }

        return new self($return);
    }

    public function current()
    {
        $array = $this->getArrayCopy();
        return reset($array);
    }

    public function removeKey($key)
    {
        return $this->offsetExists($key) ? $this->offsetUnset($key) : null;
    }

    public function moveToFront($key)
    {
        $array = $this->get($key);
        $this->removeKey($key);
        $this->prepend($array);
        return $this;
    }

    public function moveToBack($key)
    {
        $array = $this->get($key);
        $this->removeKey($key);
        $this->append($array);
        return $this;
    }

    public function moveKeyToFront($key, $value)
    {
        foreach ($this as $index => $row) {
            if (is_object($row) && $row->$key == $value) {
                $this->moveToFront($index);
                continue;
            }

            if (is_array($row) && $row[$key] == $value) {
                $this->moveToFront($index);
                continue;
            }

        }

        return $this;
    }

    public function moveKeyToBack($key, $value)
    {
        foreach ($this as $index => $row) {
            if (is_object($row) && $row->$key == $value) {
                $this->moveToBack($index);
                continue;
            }

            if (is_array($row) && $row[$key] == $value) {
                $this->moveToBack($index);
                continue;
            }

        }

        return $this;
    }

    public function getArray()
    {
        return $this->getArrayCopy();
    }

    public function contains($value)
    {
        return in_array($value, $this->getArray());
    }

    public static function setUrlFilter(array $filter)
    {
        return base64_encode(json_encode($filter));
    }

    public function filterFromUrl(string $filter): self
    {
        $filters = json_decode(base64_decode($filter), true);
        if ($filters && is_array($filters) && count($filters)) {
            foreach ($filters as $filter) {
                $this->filter($filter[0], $filter[1], $filter[2]);
            }

        }

        return $this;
    }

    public function filter($key, $operator, $value)
    {
        $this->_filtered_array = $this->getArray();
        foreach ($this->_filtered_array as $index => $item) {

            if (is_a($item, 'Collection')) {
                $item = $item->getArray();
            }

            if (is_array($item)) {
                $actual = $item[$key];
            }
            else if (is_object($item)) {
                $actual = $item->$key;
            }

            switch ($operator) {
                case '=':
                    $this->_filterEquals($value, $actual, $index);
                    break;
                case '!=':
                case '<>':
                    $this->_filterNotEqual($value, $actual, $index);
                    break;
                case '!==':
                    $this->_filterNotEqualStrict($value, $actual, $index);
                    break;
                case '===':
                    $this->_filterEqualsStrict($value, $actual, $index);
                    break;
                case 'in':
                    $this->_filterInArray($value, $actual, $index);
                    break;
                case 'not in':
                    $this->_filterNotInArray($value, $actual, $index);
                    break;
                case 'array_match':
                    $this->_filterArrayMatch($value, $actual, $index);
                    break;
                case 'is':
                    $this->_filterIsNull($value, $actual, $index);
                case 'is not':
                    $this->_filterIsNotNull($value, $actual, $index);
                    break;
                case 'like':
                    $this->_filterLike($value, $actual, $index);
                    break;
                case '>':
                    $this->_filterGreaterThan($value, $actual, $index);
                    break;
                case '<':
                    $this->_filterLessThan($value, $actual, $index);
                    break;
                case 'has value':
                    $this->_filterHasValue($value, $actual, $index);
                    break;
            }

        }

        $this->exchangeArray($this->_filtered_array);
        $this->_filtered_array = [];
        return $this;
    }

    public function removeFilter(): self
    {
        foreach ($this->_removed as $key => $row) {
            $this->append($row);
            unset($this->_removed[$key]);
        }

        return $this;
    }

    public function getKeys()
    {
        $array = $this->get(0);
        if (!$array) {
            return [];
        }

        $array = is_array($this->get(0)) ? $this->get(0) : $this->get(0)->getArray();
        return array_keys($array);
    }

    public function getFirstValue()
    {
        return reset($this->getArray());
    }

    public function getFirstKey()
    {
        return reset(array_flip($this->getArray()));
    }

    public function getLastValue()
    {
        return reset(array_reverse($this->getArray()));
    }

    public function getlasttKey()
    {
        return reset(array_flip(array_reverse($this->getArray())));
    }

    public function limit(int $limit, int $offset = 0)
    {
        return array_slice($this->getArray(), $offset, $limit);
    }

    protected function _filterEquals($value, $actual, $index)
    {
        if ($value == $actual) {
            return;
        }
        $this->_removed[] = $this->_filtered_array[$index];
        unset($this->_filtered_array[$index]);
    }

    protected function _filterNotEqual($value, $actual, $index)
    {
        if ($value != $actual) {
            return;
        }

        $this->_removed[] = $this->_filtered_array[$index];
        unset($this->_filtered_array[$index]);
    }

    protected function _filterEqualsStrict($value, $actual, $index)
    {
        if ($value === $actual) {
            return;
        }

        $this->_removed[] = $this->_filtered_array[$index];
        unset($this->_filtered_array[$index]);
    }

    protected function _filterNotEqualStrict($value, $actual, $index)
    {
        if ($value !== $actual) {
            return;
        }

        $this->_removed[] = $this->_filtered_array[$index];
        unset($this->_filtered_array[$index]);
    }

    protected function _filterNotInArray($value, $actual, $index)
    {
        if (!in_array($actual, $value)) {
            return;
        }

        $this->_removed[] = $this->_filtered_array[$index];
        unset($this->_filtered_array[$index]);
    }

    protected function _filterArrayMatch($value, $actual, $index)
    {
        if (count(array_intersect($actual, $value))) {
            return;
        }

        $this->_removed[] = $this->_filtered_array[$index];
        unset($this->_filtered_array[$index]);
    }

    protected function _filterInArray($value, $actual, $index)
    {
        if (in_array($actual, $value)) {
            return;
        }

        $this->_removed[] = $this->_filtered_array[$index];
        unset($this->_filtered_array[$index]);
    }

    protected function _filterIsNull($value, $actual, $index)
    {
        if ($actual === null) {
            return;
        }

        $this->_removed[] = $this->_filtered_array[$index];
        unset($this->_filtered_array[$index]);
    }

    protected function _filterIsNotNull($value, $actual, $index)
    {
        if ($actual !== null) {
            return;
        }

        $this->_removed[] = $this->_filtered_array[$index];
        unset($this->_filtered_array[$index]);
    }

    protected function _filterLike($value, $actual, $index)
    {
        if (strpos($actual, $value) !== false) {
            return;
        }

        $this->_removed[] = $this->_filtered_array[$index];
        unset($this->_filtered_array[$index]);
    }

    protected function _filterGreaterThan($value, $actual, $index)
    {
        if ($actual >= $value) {
            return;
        }

        $this->_removed[] = $this->_filtered_array[$index];
        unset($this->_filtered_array[$index]);
    }

    protected function _filterLessThan($value, $actual, $index)
    {
        if ($actual <= $value) {
            return;
        }

        $this->_removed[] = $this->_filtered_array[$index];
        unset($this->_filtered_array[$index]);
    }

    protected function _filterHasValue($value, $actual, $index)
    {
        if ($actual) {
            return;
        }

        $this->_removed[] = $this->_filtered_array[$index];
        unset($this->_filtered_array[$index]);
    }

    public function prepend($data)
    {
        $array = $this->getArrayCopy();
        array_unshift($array, $data);
        $this->exchangeArray($array);

        return $this;
    }

    public function sortOnKey($key, $order = self::DESC, $flag = null) {
        $sort = [];
        $missing = [];
        $smart_sort = $flag;
        foreach ($this as $index => $item) {
            $item_key;
            if (is_array($item) || is_a($item, 'ArrayObject')) {
                $item_key = $item[$key];
            }
            else if (is_object($item)) {
                $item_key = $item->$key;
            }

            if(!isset($item_key)) {
                $missing[] = $item;
                continue;
            }

            if (is_a($item_key, 'Tempus')) {
                $item_key = $item_key->mysql();
            }

            $item_key = self::_sanitizeKey($item_key);
            if (is_numeric($item_key) && $smart_sort === null) {
                $smart_sort = SORT_NUMERIC;
            }

            $sort[$item_key . '_' . $index] = $item;
        }

        ksort($sort, $smart_sort ?: SORT_REGULAR);
        $sorted = [];
        foreach ($sort as $original_key => $item) {
            $key_parts = explode('_', $original_key);
            $original_key = $key_parts[1];
            $sorted[$original_key] = $item;
        }

        $sorted = array_merge($sorted, $missing);
        if ($order == self::DESC) {
            $sorted = array_reverse($sorted, true);
        }

        $this->exchangeArray($sorted);

        return $this;
    }

    public function getJSON()
    {
        return json_encode($this->getArray());
    }

    public function paginate($page = 1, $items_per_page = 20)
    {
        $array = $this->getArray();
        $array = array_chunk($array, $items_per_page, true);
        $this->exchangeArray($array);
        $this->pages = $this->count();
        $this->current_page = $page;
        return $this;
    }

    public function getPage(int $page = 1)
    {
        return new self($this->get($page));
    }

    public function getNextPage()
    {
        return new self($this->get($this->current_page + 1));
    }

    public function getLastPage()
    {
        return new self($this->get($this->pages - 1));
    }

    public function tail(int $count = 5)
    {
        $this->exchangeArray(array_slice($this->getArray(), 0 - $count));
        return $this;
    }

    public function getCurrentPage()
    {
        $page = $this->get($this->current_page - 1);
        if (!$page) {
            return;
        }

        return new Collection($page);
    }

    public function implode(string $glue)
    {
        return implode($glue, $this->getArray());
    }

    public function merge(self $collection): self
    {
        $this->exchangeArray(array_merge($this->getArray(), $collection->getArray()));
        return $this;
    }

    public function mergeObjects(self $collection)
    {
        $existing = $this->getObjectIds();
        foreach ($collection as $item) {
            if (in_array($item->object, $existing)) {
                continue;
            }

            $this->append($item);
        }

        return $this;
    }

    public function filterNullRows(): self
    {
        foreach ($this as $k => $v) {
            if ($v === null) {
                $this->removeKey($k);
            }
        }

        return $this;
    }

    public function shuffle(): self
    {
        $array = $this->getArray();
        shuffle($array);
        $this->exchangeArray($array);
        return $this;
    }

    public function getCSV(string $file_name = null)
    {
        $file_path = SYSTEM_CACHE_DIR . '/' . ($file_name ?: (time() . rand(1000,9999))) . '.csv';
        unlink($file_path);
        
        $file = new SplFileObject($file_path, 'a');
        $file->fputcsv($this->getKeys());
        foreach($this as $row) {

            if (is_a($row, 'Collection')) {
                $row = $row->getArray();
            }

            if (!is_array($row)) {
                continue;
            }

            $file->fputcsv(array_values($row));
        }

        $return = file_get_contents($file->getPathname());
        if ($file_name) {
            return $file_path;
        }

        unlink($file->getPathname());
        return $return;
    }

    public function __call($func, $argv)
    {
        if (!is_callable($func) || substr($func, 0, 6) !== 'array_') {
            throw new BadMethodCallException(__CLASS__.'->'.$func);
        }

        return call_user_func_array($func, array_merge(array($this->getArrayCopy()), $argv));
    }

    protected static function _sanitizeKey(string $string = '') {
        $string = preg_replace('/[^\w\-]+/u', '-', $string);
        $r = mb_strtolower(preg_replace('/--+/u', '-', $string), 'UTF-8');
        $url = rtrim($r, '-');
        $url = ltrim($url, '-');
        return $url;
    }

}
